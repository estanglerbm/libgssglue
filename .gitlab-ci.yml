.artifacts:
  interruptible: true
  artifacts:
    when: always
    paths:
    - "*.tar.gz"
    - ./*.log
    - ./config.h
    - ./**/*.log

.bootstrap:
  stage: build
  extends: .artifacts
  script:
  - ./bootstrap

.tarball:
  stage: test
  extends: .artifacts
  script:
  - tar xfa libgssglue-*.tar.gz
  - cd `ls -d libgssglue-* | grep -v tar.gz`

.localchecks:
  script:
  - gcc -o a tests/generic.c -Isrc/gssglue src/.libs/libgssglue.a -ldl
  - ./a
  - gcc -o b tests/generic.c -Isrc/gssglue src/.libs/libgssglue.so -ldl
  - LD_PRELOAD=src/.libs/libgssglue.so ./b
  - make install
  - gcc -o c tests/generic.c -I/usr/local/include/gssglue -L/usr/local/lib -Wl,-rpath,/usr/local/lib -lgssglue -ldl
  - ./c

CentOS7:
  extends: .bootstrap
  image: centos:7
  before_script:
  - yum update -y | tail
  - yum -y install make gcc autoconf automake libtool | tail
  script:
  - !reference [.bootstrap, script]
  - ./configure
  - make check V=1 VERBOSE=t CPPFLAGS="-Wall"
  - make distcheck V=1
  - !reference [.localchecks, script]

Debian11:
  extends: .bootstrap
  image: debian:11-slim
  before_script:
  - apt-get update -qq | tail
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -qq autoconf automake libtool make indent git valgrind | tail
  script:
  - !reference [.bootstrap, script]
  - ./configure
  - make check V=1 VERBOSE=t CPPFLAGS="-Wall -Werror -Wno-error=incompatible-pointer-types"
  - make distcheck V=1
  - make check LOG_COMPILER="valgrind --error-exitcode=1 --leak-check=full"
  - !reference [.localchecks, script]
  - make indent
  - git diff --exit-code # nothing should change version controlled files

Gentoo:
  extends: .bootstrap
  image: gentoo/stage3
  script:
  - !reference [.bootstrap, script]
  - ./configure
  - make check V=1 VERBOSE=t CPPFLAGS="-Wall -Werror -Wno-error=incompatible-pointer-types -Wno-error=array-bounds"
  - make distcheck V=1
  - !reference [.localchecks, script]

# https://docs.gitlab.com/ee/user/application_security/sast/
sast:
  stage: build
include:
- template: Security/SAST.gitlab-ci.yml

# https://clang.llvm.org/docs/AddressSanitizer.html
# https://github.com/google/sanitizers/wiki/AddressSanitizer
# https://clang.llvm.org/docs/UndefinedBehaviorSanitizer.html
# https://clang.llvm.org/docs/LeakSanitizer.html
# https://github.com/google/sanitizers/wiki/AddressSanitizerLeakSanitizer
# https://github.com/google/sanitizers/wiki/AddressSanitizerFlags
F36-clang-ASan-UBSan:
  image: fedora:36
  stage: build
  interruptible: true
  before_script:
  - dnf update -y | tail
  - dnf -y install make clang autoconf automake libtool | tail
  script:
  - ./bootstrap
  - ./configure CC=clang CFLAGS="-g -O1 -fsanitize=undefined -fsanitize=address -fsanitize-address-use-after-scope -fno-sanitize-recover=undefined -fno-omit-frame-pointer -fno-optimize-sibling-calls"
  - ASAN_OPTIONS=detect_stack_use_after_return=1:check_initialization_order=1:strict_init_order=1 make check VERBOSE=t V=1

# https://www.synopsys.com/blogs/software-security/integrating-coverity-scan-with-gitlab-ci/
Coverity:
  stage: build
  image: debian:11-slim
  only:
    refs:
      - master
      - coverity
  allow_failure: true
  interruptible: true
  before_script:
  - apt-get update -qq | tail
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -qq autoconf automake libtool make indent git curl | tail
  script:
  - test -n "$COVERITY_SCAN_TOKEN" && test -n "$COVERITY_SCAN_PROJECT_NAME"
  - curl -o /tmp/cov-analysis-linux64.tgz https://scan.coverity.com/download/linux64
    --form project=$COVERITY_SCAN_PROJECT_NAME --form token=$COVERITY_SCAN_TOKEN
  - sha1sum /tmp/cov-analysis-linux64.tgz
  - tar xfz /tmp/cov-analysis-linux64.tgz
  - time ./bootstrap
  - time ./configure CFLAGS="-g -Og"
  - cov-analysis-linux64-*/bin/cov-build --dir cov-int make check -j$(nproc)
  - tar cfz cov-int.tar.gz cov-int
  - curl https://scan.coverity.com/builds?project=$COVERITY_SCAN_PROJECT_NAME
    --form token=$COVERITY_SCAN_TOKEN --form email=$GITLAB_USER_EMAIL
    --form file=@cov-int.tar.gz --form version="`git describe --tags`"
    --form description="`git describe --tags` / $CI_COMMIT_TITLE / $CI_COMMIT_REF_NAME:$CI_PIPELINE_ID"
  artifacts:
    expire_in: 1 week
    paths:
      - cov-int/*.txt

AlmaLinux8:
  extends: .tarball
  image: almalinux:8
  needs: [Debian11]
  before_script:
  - yum -y install make gcc | tail
  script:
  - !reference [.tarball, script]
  - ./configure
  - make check V=1 VERBOSE=t CPPFLAGS="-Wall -Werror -Wno-error=incompatible-pointer-types"
  - make distcheck V=1
  - !reference [.localchecks, script]

Ubuntu2204:
  extends: .tarball
  image: ubuntu:22.04
  needs: [Debian11]
  before_script:
  - apt-get update -qq | tail
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -qq make gcc | tail
  script:
  - !reference [.tarball, script]
  - ./configure
  - make check V=1 VERBOSE=t CPPFLAGS="-Wall -Werror -Wno-error=incompatible-pointer-types -Wno-error=array-bounds"
  - make distcheck V=1
  - !reference [.localchecks, script]

ArchLinux-tcc:
  extends: .tarball
  image: archlinux:latest
  needs: [Debian11]
  before_script:
  - pacman -Syu --noconfirm make tcc binutils | tail
  script:
  - !reference [.tarball, script]
  - ./configure CC=tcc
  - make check V=1 VERBOSE=t CPPFLAGS="-Wall"
  - make distcheck V=1 DISTCHECK_CONFIGURE_FLAGS='CC=tcc'
  - tcc -o a tests/generic.c -Isrc/gssglue src/.libs/libgssglue.a -ldl
  - ./a
  - tcc -o b tests/generic.c -Isrc/gssglue src/.libs/libgssglue.so -ldl
  - LD_PRELOAD=src/.libs/libgssglue.so ./b
  - make install
  - tcc -o c tests/generic.c -I/usr/local/include/gssglue -L/usr/local/lib -Wl,-rpath,/usr/local/lib -lgssglue -ldl
  - ./c

Fedora36-clang:
  extends: .tarball
  image: fedora:36
  needs: [Debian11]
  before_script:
  - dnf -y install make clang | tail
  script:
  - !reference [.tarball, script]
  - ./configure CC=clang
  - make check V=1 VERBOSE=t CPPFLAGS="-Wall -Werror -Wno-error=incompatible-pointer-types"
  - make distcheck V=1 DISTCHECK_CONFIGURE_FLAGS='CC=clang'
  - clang -o a tests/generic.c -Isrc/gssglue src/.libs/libgssglue.a -ldl
  - ./a
  - clang -o b tests/generic.c -Isrc/gssglue src/.libs/libgssglue.so -ldl
  - LD_PRELOAD=src/.libs/libgssglue.so ./b
  - make install
  - clang -o c tests/generic.c -I/usr/local/include/gssglue -L/usr/local/lib -Wl,-rpath,/usr/local/lib -lgssglue -ldl
  - ./c

D-coverage:
  extends: .bootstrap
  image: debian:testing-slim # for lcov 1.16
  stage: build
  before_script:
  - apt-get update -qq | tail
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -qq autoconf automake libtool make indent git lcov | tail
  script:
  - ./bootstrap
  - ./configure
  - lcov --directory . --zerocounters
  - make check CFLAGS="-g --coverage" VERBOSE=t
  - mkdir -p coverage
  - lcov --directory . --output-file coverage/libgssglue.info --capture
  - genhtml --output-directory coverage coverage/libgssglue.info
            --highlight --frames --legend --title "Libgssglue"
  - make indent
  - git diff --exit-code # nothing should change version controlled files
  artifacts:
    when: on_success
    paths:
      - coverage

.pages:
  stage: deploy
  needs: ["D-coverage"]
  script:
    - mkdir public
    - mv coverage/ public/
  artifacts:
    paths:
    - public
    expire_in: 30 days

pages:
  extends: .pages
  only:
    - master

pages-test:
  extends: .pages
  except:
    - master
